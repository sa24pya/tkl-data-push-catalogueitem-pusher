<?php
if ($_POST) {
    try {
        if (array_key_exists('data', $_POST)) {
            $productItems = json_decode($_POST['data'], true);
            echo json_encode([
                "productCount" => count($productItems),
                "items" => $productItems,
                "timestamp" => time(),
            ]);
        } else {
            echo json_encode([
                "error" => "Missing data!",
                "timestamp" => time(),
            ]);
        }
    } catch (\Exception $e) {
        print_r("An error happened!\n");
        print_r($e->getMessage());
    }
} else {
    echo "This file should be called from _POST[]";
}
