<?php
/**
 * File: Pusher.php
 *
 * @author Jesus Coll Cerdan jesus.coll@basetis.com, bcn
 * @version 1.0
 * @package application
 */
namespace DataPushCatalogueItemPusher;

/**
 * Class Pusher
 * @package application
 * @subpackage DataAccess
 */
use Dotenv\Dotenv;
use DataAccessDAL\DAL;

class CatalogueItemPusher
{
    public static function push(array $catalogueItems)
    {
        $dotenv = new Dotenv(dirname(__DIR__));
        $dotenv->load();
        $url = getenv('MAGMI_URL');
        $data = "data=".json_encode($catalogueItems);

        $options = array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_USERAGENT      => "test", // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POSTFIELDS     => $data
        );

        //open connection
        $ch = curl_init();
        curl_setopt_array($ch, $options);

        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        //execute post
        $result = curl_exec($ch);
        $errors = curl_error($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $gtins = [];
        foreach ($catalogueItems as $catalogueItem) {
            $gtins[] = $catalogueItem['gtin'];
        }
        //close connection
        curl_close($ch);
        if ($response === 200) {
            DAL::getInstance()->setSA24PushPendingFalse($gtins);
        }
        echo "\n".$result;
        echo "\n".$errors;
        echo "\n".$response;
        echo "\n";
        return ($result);
    }
}
