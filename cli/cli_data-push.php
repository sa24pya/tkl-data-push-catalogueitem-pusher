<?php
/**
 * M4 Dispatcher
 * The service to be called, the "service call name", is sent
 * within POST parameters.
 *
 * @author mrx
 * @version 1.0
 * @package application
 * @see https://docs.google.com/a/shopall24.com/document/d/1z1ekPxx0HVi_8YtbMuZefarKlmxIsdjxIpEfJYFa8RA/edit?usp=sharing
 */

namespace DataPushCatalogueItemPusher\cli;

// Free spech https://github.com/Behat/Behat/blob/master/bin/behat
if (is_file($autoload = getcwd() . '/vendor/autoload.php')) {
    require $autoload;
}

if (is_file($autoload = __DIR__ . '/../vendor/autoload.php')) {
    require($autoload);
} elseif (is_file($autoload = __DIR__ . '/../../../autoload.php')) {
    require($autoload);
} else {
    fwrite(
        STDERR,
        'You must set up the project dependencies, run the following commands:'.PHP_EOL.
        'curl -s http://getcomposer.org/installer | php'.PHP_EOL.
        'php composer.phar install'.PHP_EOL
    );
    exit(1);
}

use DataPushCatalogueItemCollector\CatalogueItemCollector;
use DataPushCatalogueItemTransformer\CatalogueItemTransformer;
use DataPushCatalogueItemPusher\CatalogueItemPusher;
use DataAccessDAL\DAL;
use Dotenv\Dotenv;

// COLLECTOR call
$col = new CatalogueItemCollector();
$push = new CatalogueItemPusher();
$dotenv = new Dotenv(dirname(__DIR__));
$trans = new CatalogueItemTransformer();
$dotenv->load();
DAL::getInstance()->setCollections(getenv('MONGOCOLLECTIONNAME'), getenv('MONGOCOLLECTIONSA24NAME'));
$result = $col->collect();
if ($result) {
    $merged = $trans->transform($result);
    $failed = $trans->failedGTINs;
    $push->push($merged);
    echo "Failed GTINs: ".count($failed)."\n";
    if (!empty($failed)) {
        echo implode(', ', $failed);
    }
} else {
    echo "The collection doesn't have any pending catalogueItems to push.\n";
}
