<?php
namespace DataPushCatalogueItemPusher\tests;

use DataPushCatalogueItemPusher\CatalogueItemPusher;

class CatalogueItemPusherTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
    }
    public function tearDown()
    {
    }
    private function generateAnArray($qty = 10)
    {
        $items = [];
        for ($i=0; $i < $qty; $i++) {
            $items[] = [
                "GTIN" => "b$i",
            ];
        }
        return $items;
    }

    public function testHeaderCommented()
    {
        echo "-------------------------------------------------------------------\n";
        echo " WARNING!!! \n The TEST will FAIL \n"
            . " if the src/helpers/MicroServer.php is not launched \n"
            . " on port :7001 via php test server (php -S localhost:7001) \n" ;
        echo "-------------------------------------------------------------------\n";
    }
    public function testPush()
    {
        $qty = 10;
        $rawData = CatalogueItemPusher::push($this->generateAnArray($qty));
        $response = json_decode($rawData, true);
        $this->assertTrue($response['productCount'] === $qty);
    }
}
